Commerce Cart Empty
-------------------
Provides a way for the current user's cart to be emptied.


Configuration
--------------------------------------------------------------------------------
Once enabled, an option will be available on the checkout configuration page
(admin/commerce/config/checkout) to control the path used to trigger the cart
emptying.


Credits / Contact
--------------------------------------------------------------------------------
Written and maintained by:
 Damien McKenna [1].
 Bram Driesen [2].

The best way to contact the author is to submit an issue, be it a support
request, a feature request or a bug report, in the project's issue queue:
  https://www.drupal.org/project/issues/commerce_cart_empty


References
--------------------------------------------------------------------------------
1: https://www.drupal.org/u/damienmckenna
2: https://www.drupal.org/u/bramdriesen
