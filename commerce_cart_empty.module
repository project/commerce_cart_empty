<?php
/**
 * @file
 * Primary hook implmentations for Commerce Cart Empty.
 */

/**
 * Implements hook_menu().
 */
function commerce_cart_empty_menu() {
  $path = variable_get('commerce_cart_empty_path', 'cart-clear');
  $items[$path] = array(
    'page callback' => 'commerce_cart_empty_cart_clear',
    'access arguments' => array('access checkout'),
    'type' => MENU_CALLBACK,
    'file' => 'commerce_cart_empty.pages.inc',
  );

  return $items;
}

/**
 * Implements hook_form_FORM_ID_alter() for commerce_checkout_builder_form().
 */
function commerce_cart_empty_form_commerce_checkout_builder_form_alter(&$form, $form_state, $form_id) {
  $form['commerce_cart_empty'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cart empty'),
    'commerce_cart_empty_path' => array(
      '#type' => 'textfield',
      '#title' => t("Path that will empty the current user's cart"),
      '#description' => t('A user loading this path will have their cart emptied..'),
      '#default_value' => variable_get('commerce_cart_empty_path', 'cart-clear'),
      '#required' => TRUE,
    ),
  );

  $form['actions']['#weight'] = 10;
  $form['actions']['submit']['#submit'][] = 'commerce_cart_empty_form_commerce_checkout_builder_form_submit';
}

/**
 * Custom submit function to save module settings after form submit.
 *
 * @see commerce_cart_empty_form_commerce_checkout_builder_form_alter().
 */
function commerce_cart_empty_form_commerce_checkout_builder_form_submit($form, $form_state) {
  // Work out what the current setting is.
  $existing_value = variable_get('commerce_cart_empty_path', 'cart-clear');

  // The new path.
  $new_value = $form_state['values']['commerce_cart_empty_path'];

  // Reload the menus if the setting changed.
  if ($existing_value != $new_value) {
    variable_set('commerce_cart_empty_path', $new_value);
    variable_set('menu_rebuild_needed', TRUE);
  }
}
